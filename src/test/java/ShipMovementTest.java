import object.Ship;
import object.ShipPositon;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipMovementTest {

    @Test
    void moveShipInSectorRange() {
        /*given*/
        Ship ship = new Ship();
        ship.setPosition(0,0,0,0);

        /*when*/

        //7 field right, 10 field up
        ship.move(7,10);

        /*then*/

        ShipPositon shipPositon = ship.getPosition();
        assertEquals(0,shipPositon.getQuadrantX());
        assertEquals(0,shipPositon.getQuadrantY());
        assertEquals(7,shipPositon.getSectorX());
        assertEquals(10,shipPositon.getSectorY());
    }

    @Test
    @Disabled
    void moveShipToOtherQuadrant(){
        /*given*/
        /*when*/
        /*then*/
    }
}
