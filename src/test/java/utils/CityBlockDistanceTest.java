package utils;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CityBlockDistanceTest {

    @Test
    void getCityBlockDistance() {
        //given
        int[] testValues1 = {0, 0, 1, 1, 2};
        int[] testValues2 = {0, 0, -1, -1, 2};
        int[] testValues3 = {0, 0, 0, 0, 0};

        //when

        CityBlockDistance cityBlockDistance = new CityBlockDistance();
        int test1 = cityBlockDistance.getCityBlockDistance(testValues1[0], testValues1[1], testValues1[2], testValues1[3]);
        int test2 = cityBlockDistance.getCityBlockDistance(testValues2[0], testValues2[1], testValues2[2], testValues2[3]);
        int test3 = cityBlockDistance.getCityBlockDistance(testValues3[0], testValues3[1], testValues3[2], testValues3[3]);

        //then
        assertEquals(test1, testValues1[4]);
        assertEquals(test2,testValues2[4]);
        assertEquals(test3, testValues3[4]);
    }
}