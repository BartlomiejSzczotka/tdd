package object;

public class ShipPositon {
    private int qx, qy, sx, sy;

    public ShipPositon(int quadrantX, int quadrantY, int sectorX, int sectorY){

        this.qx = quadrantX;
        this.qy = quadrantY;
        this.sx = sectorX;
        this.sy = sectorY;
    }

    public void setQuadrantX(int x1) {
        this.qx = x1;
    }

    public void setQuadrantY(int y1) {
        this.qy = y1;
    }

    public void setSectorX(int x2) {
        this.sx = x2;
    }

    public void setSectorY(int y2) {
        this.sy = y2;
    }

    public int getQuadrantX() {
        return qx;
    }

    public int getQuadrantY() {
        return qy;
    }

    public int getSectorX() {
        return sx;
    }

    public int getSectorY() {
        return sy;
    }
}
