package object;

public class Ship {
    private ShipPositon shipPositon;

    public Ship(){
        shipPositon = new ShipPositon(0, 0, 0, 0);
    }

    public void move(int x, int y) {
        shipPositon.setSectorX(x);
        shipPositon.setSectorY(y);
    }

    public void setPosition(int qx, int qy, int sx, int sy) {
        shipPositon.setQuadrantX(qx);
        shipPositon.setQuadrantY(qy);
        shipPositon.setSectorX(sx);
        shipPositon.setSectorY(sy);
    }

    public ShipPositon getPosition() {
        return shipPositon;
    }
}
